package mx.tecnm.misantla.appitsav4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import mx.tecnm.misantla.appitsav4.databinding.ActivityMainBinding
import mx.tecnm.misantla.appitsav4.ui.fragments.CampusFragment
import mx.tecnm.misantla.appitsav4.ui.fragments.HomeFragment
import mx.tecnm.misantla.appitsav4.ui.fragments.InformaFragment

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        loadFragments(HomeFragment())

        binding.bottomNavigationView.setOnNavigationItemSelectedListener {
          when{
              it.itemId == R.id.navigationHome ->{
                  loadFragments(HomeFragment())
                  return@setOnNavigationItemSelectedListener true
              }
              it.itemId == R.id.navigationCampus ->{
                  loadFragments(CampusFragment())
                  return@setOnNavigationItemSelectedListener true
              }
              it.itemId == R.id.navigationInforma ->{
                  loadFragments(InformaFragment())
                  return@setOnNavigationItemSelectedListener true
              }

              else ->{
                  return@setOnNavigationItemSelectedListener false
              }
          }
        }

    }

    private fun loadFragments(fragment:Fragment){
     supportFragmentManager.beginTransaction().also {
         it.replace(R.id.fragmetContainer,fragment)
         it.commit()
     }
    }
}